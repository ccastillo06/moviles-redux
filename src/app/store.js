import { configureStore } from '@reduxjs/toolkit';

import catalogReducer from '../features/catalog/catalogSlice';

// La store es el lugar donde Redux guardará el estado de mi aplicación y todos los reducers que usa
// para cambiar el estado de forma previsible.
const store = configureStore({
  reducer: {
    catalog: catalogReducer,
  },
});

export default store;
