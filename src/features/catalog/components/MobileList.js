import React, { useEffect } from 'react';
import { useSelector } from 'react-redux';
import { Link } from 'react-router-dom';

import MobileCard from './MobileCard';
import { selectMobiles } from '../catalogSlice';

export default function MobileList() {
  const mobiles = useSelector(selectMobiles);

  return (
    <div>
      <h1>Catálogo de móviles</h1>

      <Link to="/new">Crear un nuevo móvil</Link>

      <ul>
        {mobiles.map((mobile) => (
          <MobileCard
            key={mobile.id}
            id={mobile.id}
            name={mobile.name}
            brand={mobile.brand}
            description={mobile.description}
            price={mobile.price}
            pixels={mobile.pixels}
            inches={mobile.inches}
          />
        ))}
      </ul>
    </div>
  );
}
