// Esto es como una petición falsa a una API que devuelve un array de móviles
export const fakeFetch = async () => {
  return new Promise((resolve) => {
    setTimeout(() => {
      resolve([
        {
          id: '1',
          name: 'Nombre - 1',
          brand: 'Marca - 1',
          description: 'Descripción - 1',
          price: '1',
        },
        {
          id: '10',
          name: 'Nombre - 10',
          brand: 'Marca - 10',
          description: 'Descripción - 10',
          price: '10',
        },
      ]);
    }, 500);
  });
};
